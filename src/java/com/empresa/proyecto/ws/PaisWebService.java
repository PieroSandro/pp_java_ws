/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.empresa.proyecto.ws;

import com.empresa.proyecto.app.Pais;
import com.empresa.proyecto.service.PaisService;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author Piero
 */
@WebService(serviceName = "PaisWebService")
public class PaisWebService {

    PaisService paisService=new PaisService();
    
    @WebMethod(operationName = "crearPaises")
    public String crearPaises(@WebParam(name = "nomPais") String nomPais) {
        return paisService.crearPais(nomPais);
    }
    
    @WebMethod(operationName = "listarPais")
    public List<Pais> listarPaises() {
        return paisService.listarPais();
    }
}
