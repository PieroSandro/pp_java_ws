
package com.empresa.proyecto.service;

import com.empresa.proyecto.app.Pais;
import java.util.ArrayList;
import java.util.List;

public class PaisService {
    
    List<Pais> listaPaises=new ArrayList<>();
    
    public PaisService(){
        
    }
    
    public String crearPais(String nombrePais){
        Pais p=new Pais();
        p.setIdPais(listaPaises.size()+1);
        p.setNombrePais(nombrePais);
        listaPaises.add(p);
        return "Se agrego "+p.getNombrePais();
    }
    
    public List<Pais> listarPais(){
        return listaPaises;
    }
}
